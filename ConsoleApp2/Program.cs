﻿using Plafi.DynamicLinq;
using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<A, string, bool> func = Test;
            Func<A, string, bool> exp = (obj, searchText) => obj.B == null || obj.B.StartsWith(searchText);

            var test = new FilterContext<A>();
            test.AddFilter("#", (item, str) => item.B.Contains(str), isDefault: true)
                .AddFilter("$", (obj, str) => obj.B.StartsWith(str))
                .AddLocalization(LogicOperator.OR, "или");

            while (true)
            {
                var line = Console.ReadLine();
                var ttt = test.GetExpression(line);
            }
        }

        private static bool Test(A item, string text)
        {
            if (item != null)
            {
                if (item.B != null)
                {
                    if (text.Contains(item.B))
                    {
                        return true;
                    }
                }
            }

            return false;
        }


    }

    public class A
    {
        public string B { get; set; }
        public List<string> List { get; set; }
    }
}
