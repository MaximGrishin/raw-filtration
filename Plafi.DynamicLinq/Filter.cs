﻿using System;
using System.Linq.Expressions;

namespace Plafi.DynamicLinq
{
    internal class Filter<T>
    {
        public Filter(string token,
                      string filterSymbol,
                      Expression<Func<T, string, bool>> expression, 
                      string delimeter,
                      bool delimiterOptional = true,
                      bool isDefault = false)
        {
            Token = token;
            FilterSymbol = filterSymbol;
            Expression = expression;
            Delimeter = delimeter;
            IsDelimiterOptional = delimiterOptional;
            IsDefault = isDefault; 
        }

        public string Token { get; }
        public string FilterSymbol { get; }
        public bool IsDefault { get; }
        public Expression<Func<T, string, bool>> Expression { get; }
        public string Delimeter { get; }
        public bool IsDelimiterOptional { get; }
    }
}