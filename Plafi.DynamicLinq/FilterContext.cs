﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace Plafi.DynamicLinq
{
    public class FilterContext<T>
    {
        public LogicOperator DefaultOperator { get; set; } = LogicOperator.OR;

        internal readonly List<Filter<T>> filters = new List<Filter<T>>();
        private readonly Dictionary<LogicOperator, string> reservedTokens = Enum.GetValues(typeof(LogicOperator)).Cast<LogicOperator>().ToDictionary(x => x, x => Enum.GetName(typeof(LogicOperator), x));

        public Expression<Func<T, bool>> GetExpression(string query)
        {
            var parameters = new Dictionary<string, string>();
            var expression = ParseString(query, parameters);

            var funcParameters = new Regex("~[0-9]+").Matches(expression).Cast<Match>().Select(x => x.Value).Distinct().OrderBy(x => x);

            List<object> result = new List<object>();

            int i = 0;
            foreach (var param in funcParameters)
            {
                result.Add(filters.Single(x => x.FilterSymbol == param).Expression);
                expression = expression.Replace(param, $"@{i}");
                i++;
            }

            foreach (var param in parameters)
            {
                result.Add(param.Value);
                expression = expression.Replace(param.Key, $"@{i}");
                i++;
            }

            return DynamicExpressionParser.ParseLambda<T, bool>(ParsingConfig.DefaultEFCore21, false, expression, result.ToArray());
        }

        private string ParseString(string filterText, Dictionary<string, string> parameters)
        {
            bool wasFunction = false;
            var sb = new StringBuilder();
            var wordsInFilter = filterText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string entry = null;
            bool compositeString = false;

            for (int i = 0; i < wordsInFilter.Length; i++)
            {
                if (compositeString)
                {
                    entry += ' ' + wordsInFilter[i];

                    if (wordsInFilter[i].EndsWith("\"") || wordsInFilter[i].EndsWith("\")"))
                    {
                        compositeString = false;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    entry = wordsInFilter[i];

                    if (StartsWithQuoteIgnoreToken(wordsInFilter[i]))
                    {
                        if (!wordsInFilter[i].EndsWith("\"") && !wordsInFilter[i].EndsWith("\")"))
                        {
                            compositeString = true;
                            continue;
                        }
                    }
                }

                bool startsWithBracket = entry.StartsWith("(");
                bool endsWithBracket = entry.EndsWith(")");

                var line = entry.Trim(new[] { '(', ')' }).Replace("\"", "");
                var lineNormalized = line.ToUpperInvariant();

                if (startsWithBracket)
                {
                    sb.Append("(");
                }

                if (reservedTokens.Any(x => x.Value == lineNormalized))
                {
                    var res = reservedTokens.FirstOrDefault(x => x.Value == lineNormalized).Key;
                    sb.Append($"{Enum.GetName(typeof(LogicOperator), res)}");
                    wasFunction = false;
                }
                else
                {
                    // Оператор по умолчанию
                    if (wasFunction)
                    {
                        sb.Append($"{DefaultOperator} ");
                    }

                    wasFunction = true;

                    var tokenFilters = filters.Where(x => x.IsDelimiterOptional ? lineNormalized.StartsWith(x.Token) : lineNormalized.StartsWith(x.Token + x.Delimeter));

                    if (tokenFilters.Count() == 1)
                    {
                        var filter = tokenFilters.First();

                        // Убираем из строки токен и отправляем её как параметр
                        // Сам токен
                        line = line.Substring(filter.Token.Length);

                        if (filter.Delimeter != null && line.StartsWith(filter.Delimeter))
                        {
                            line = line.Substring(filter.Delimeter.Length);
                        }

                        sb.Append($"{filter.FilterSymbol}(it, %{i}%)");
                        parameters.Add($"%{i}%", line);
                    }
                    else
                    {
                        var filter = filters.FirstOrDefault(x => x.IsDefault);

                        if (filter != null)
                        {
                            sb.Append($"{filter.FilterSymbol}(it, %{i}%)");
                            parameters.Add($"%{i}%", line);
                        }
                        else
                        {
                            throw new InvalidOperationException("Provided text has token missing and default filter is not set.");
                        }
                    }
                }

                if (endsWithBracket)
                {
                    sb.Append(")");
                }

                sb.Append(" ");

            }

            return sb.ToString();
        }

        private bool StartsWithQuoteIgnoreToken(string line)
        {
            int skip = 0;

            if (line.First() == '(')
            {
                line = line.Substring(1);
            }

            foreach (var token in filters)
            {
                if (line.StartsWith(token.IsDelimiterOptional ? token.Token : token.Token + token.Delimeter))
                {
                    skip += token.IsDelimiterOptional ? token.Token.Length : (token.Token + token.Delimeter).Length;

                    if (token.IsDelimiterOptional && token.Delimeter != null && line.StartsWith(token.Delimeter))
                    {
                        skip += token.Delimeter.Length;
                    }
                }
            }

            return line.Skip(skip).First() == '"';
        }

        public FilterContext<T> AddLocalization(LogicOperator logicOperator, string localizedValue)
        {
            reservedTokens[logicOperator] = localizedValue.ToUpper();

            return this;
        }

        public FilterContext<T> AddFilter(string filter,
                                          Expression<Func<T, string, bool>> expression,
                                          string delimeter = null,
                                          bool delimeterOptional = true,
                                          bool isDefault = false)
        {
            // There must be only one default fitler
            if (isDefault && filters.Any(x => x.IsDefault))
            {
                throw new InvalidOperationException("Default filter is already set!");
            }

            var funcName = "~" + filters.Count;

            Filter<T> newFilter = new Filter<T>(filter, funcName, expression, delimeter, delimeterOptional, isDefault);
            filters.Add(newFilter);

            return this;
        }
    }
}
