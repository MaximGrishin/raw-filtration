﻿namespace Plafi.DynamicLinq
{
    public enum LogicOperator
    {
        OR,
        AND,
        NOT
    }
}