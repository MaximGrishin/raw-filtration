﻿using System;
using System.Linq.Expressions;

namespace Plafi
{
    internal class Filter<T>
    {
        public Filter(string token, 
                      Func<T, string, bool> expression, 
                      string delimeter,
                      bool delimiterOptional = true,
                      bool isDefault = false)
        {
            Token = token;
            Expression = expression;
            Delimeter = delimeter;
            IsDelimiterOptional = delimiterOptional;
            IsDefault = isDefault; 
        }

        public string Token { get; }
        public bool IsDefault { get; }
        public Func<T, string, bool> Expression { get; }
        public string Delimeter { get; }
        public bool IsDelimiterOptional { get; }
    }
}