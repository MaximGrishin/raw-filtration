﻿using Flee.PublicTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plafi
{
    public abstract class FilterBase<T>
    {
        protected const string itemToken = "item";
        public LogicOperator DefaultOperator { get; set; } = LogicOperator.OR;

        internal readonly List<Filter<T>> filters = new List<Filter<T>>();
        private readonly IEnumerable<string> reservedTokens = Enum.GetNames(typeof(LogicOperator));

        protected string ParseString(string filterText, ExpressionContext context)
        {
            bool wasFunction = false;
            var sb = new StringBuilder();
            var wordsInFilter = filterText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string entry = null;
            bool compositeString = false;

            for (int i = 0; i < wordsInFilter.Length; i++)
            {
                if (compositeString)
                {
                    entry += ' ' + wordsInFilter[i];

                    if (wordsInFilter[i].EndsWith("\"") || wordsInFilter[i].EndsWith("\")"))
                    {
                        compositeString = false;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    entry = wordsInFilter[i];

                    if (StartsWithQuoteIgnoreToken(wordsInFilter[i]))
                    {
                        if (!wordsInFilter[i].EndsWith("\"") && !wordsInFilter[i].EndsWith("\")"))
                        {
                            compositeString = true;
                            continue;
                        }
                    }
                }

                string parameter = $"val{i}";
                string funcParameter = $"func{i}";
                bool startsWithBracket = entry.StartsWith("(");
                bool endsWithBracket = entry.EndsWith(")");

                var line = entry.Trim(new[] { '(', ')' }).Replace("\"", "");
                var lineNormalized = line.ToUpperInvariant();

                if (startsWithBracket)
                {
                    sb.Append("(");
                }

                if (reservedTokens.Contains(lineNormalized))
                {
                    sb.Append($"{line}");
                    wasFunction = false;
                }
                else
                {
                    // Оператор по умолчанию
                    if (wasFunction)
                    {
                        sb.Append($"{DefaultOperator} ");
                    }

                    wasFunction = true;

                    var tokenFilters = filters.Where(x => x.IsDelimiterOptional ? lineNormalized.StartsWith(x.Token) : lineNormalized.StartsWith(x.Token + x.Delimeter));

                    if (tokenFilters.Count() == 1)
                    {
                        var filter = tokenFilters.First();

                        // Убираем из строки токен и отправляем её как параметр
                        // Сам токен
                        line = line.Substring(filter.Token.Length);

                        if (filter.Delimeter != null && line.StartsWith(filter.Delimeter))
                        {
                            line = line.Substring(filter.Delimeter.Length);
                        }

                        context.Variables.Add(parameter, line);
                        context.Variables.Add(funcParameter, filter.Expression);
                        sb.Append($"functions.FuncExecute({funcParameter}; {itemToken}; {parameter})");
                    }
                    else
                    {
                        var filter = filters.FirstOrDefault(x => x.IsDefault);

                        if (filter != null)
                        {
                            context.Variables.Add(parameter, line);
                            context.Variables.Add(funcParameter, filter.Expression);
                            sb.Append($"functions.FuncExecute({funcParameter}; {itemToken}; {parameter})");
                        }
                        else
                        {
                            throw new InvalidOperationException("Provided text has token missing and default filter is not set.");
                        }
                    }
                }

                if (endsWithBracket)
                {
                    sb.Append(")");
                }

                sb.Append(" ");

            }

            return sb.ToString();
        }

        private bool StartsWithQuoteIgnoreToken(string line)
        {
            int skip = 0;

            if (line.First() == '(')
            {
                line = line.Substring(1);
            }

            foreach (var token in filters)
            {
                if (line.StartsWith(token.IsDelimiterOptional ? token.Token : token.Token + token.Delimeter))
                {
                    skip += token.IsDelimiterOptional ? token.Token.Length : (token.Token + token.Delimeter).Length;

                    if (token.IsDelimiterOptional && token.Delimeter != null && line.StartsWith(token.Delimeter))
                    {
                        skip += token.Delimeter.Length;
                    }
                }
            }

            return line.Skip(skip).First() == '"';
        }
    }
}
