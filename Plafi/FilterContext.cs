﻿using Flee.PublicTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Plafi
{
    public class FilterContext<T> : FilterBase<T>
    {
        public bool IsExpressionBuilt => expression != null;

        private IGenericExpression<bool> expression;

        public FilterContext<T> AddFilter(string filter,
                                          Func<T, string, bool> expression,
                                          string delimeter = null,
                                          bool delimeterOptional = true,
                                          bool isDefault = false)
        {
            // There must be only one default fitler
            if (isDefault && filters.Any(x => x.IsDefault))
            {
                throw new InvalidOperationException("Default filter is already set!");
            }

            var funcName = "function" + filters.Count;

            Filter<T> newFilter = new Filter<T>(filter, expression, delimeter, delimeterOptional, isDefault);
            filters.Add(newFilter);

            return this;
        }

        public void BuildExpression(string filterText)
        {
            var context = new ExpressionContext();
            context.Imports.AddType(typeof(FuncExecutor<T>), "functions");
            context.Variables.DefineVariable(itemToken, typeof(T));
            var parsedExpression = ParseString(filterText, context);
            expression = context.CompileGeneric<bool>(parsedExpression);
        }

        public bool Filter(T item)
        {
            if (!IsExpressionBuilt) throw new InvalidOperationException("Expression must be built before filtering!");

            expression.Context.Variables[itemToken] = item;
            return expression.Evaluate();
        }
    }
}
