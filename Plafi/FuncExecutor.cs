﻿using System;

namespace Plafi
{
    public static class FuncExecutor<T>
    {
        public static bool FuncExecute(Func<T, string, bool> function, T item, string text)
        {
            return function(item, text);
        }
    }
}
