﻿namespace Plafi
{
    public enum LogicOperator
    {
        OR,
        AND,
        NOT
    }
}