using Plafi;
using System;
using Xunit;

namespace Tests
{
    public class A
    {
        public string B { get; set; }
    }

    public class UnitTest1
    {
        private FilterContext<A> CreateContext()
        {
            Func<A, string, bool> exp = (obj, searchText) => obj.B != null && obj.B.StartsWith(searchText);

            var test = new FilterContext<A>();
            test.AddFilter("#", exp, isDefault: true);

            return test;
        }

        [Fact]
        public void Test1()
        {

        }
    }
}
